import chai, { expect } from 'chai';
import Square from '../src/square';

describe('Square', () => {
    it('should return area as 81 when side is 9', () => {
      expect(new Square(9).area()).to.eq(81);
    })
    it('should return area 64 as side is 8', () => {
      expect(new Square(8).area()).to.eq(64);
    })
  })
