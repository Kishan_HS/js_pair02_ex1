import chai, { expect } from 'chai';
import Rectangle from '../src/rectangle';

  describe('Rectangle', () => {
    it('should return area as 9 when length is 9 and width is 1', () => {
      expect(new Rectangle(9,1).area()).to.eq(9);
    })
    it('should return perimeter as 20 when length is 9 and width is 1', () => {
      expect(new Rectangle(9,1).perimeter()).to.eq(20);
    })
  })
